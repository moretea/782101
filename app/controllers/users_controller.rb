class UsersController < ApplicationController
  def edit
    @user = User.find params[:id]
    what = params[:what]
    if ["birth_day", "location"].member?(what)
      render :action => "edit_#{what}"
    else
      redirect_to :action => :show
    end
  end

  def update
    @user = User.find params[:id]
    if @user.update_attributes(params[:user])
      flash[:succes] = "Your profile has been updated."
      redirect_to :action => :show
    else
      render :action => params[:what]
    end
  end
end